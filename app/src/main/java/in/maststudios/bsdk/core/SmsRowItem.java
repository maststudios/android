package in.maststudios.bsdk.core;

import java.util.LinkedList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by tanay.soley on 14/05/16.
 */

public class SmsRowItem {
    public SmsRowItem(){
        smsColumnItems = new LinkedList<>();
    }

    private List<SmsColumnItem> smsColumnItems;

    public List<SmsColumnItem> getSmsColumnItems() {
        return smsColumnItems;
    }

    public void setSmsColumnItems(List<SmsColumnItem> smsColumnItems) {
        this.smsColumnItems = smsColumnItems;
    }
}

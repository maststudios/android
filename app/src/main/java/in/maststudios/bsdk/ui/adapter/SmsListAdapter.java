package in.maststudios.bsdk.ui.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import in.maststudios.bsdk.R;
import in.maststudios.bsdk.core.BetterSms;

/**
 * Created by mishrabhilash on 5/14/16.
 */
public class SmsListAdapter extends  RecyclerView.Adapter<SmsListAdapter.ViewHolder> {

    ArrayList<BetterSms> betterSmsList;
    WeakReference<Context> context;
    LinearLayoutManager linearLayoutManager;

    public SmsListAdapter(Context ctx,ArrayList<BetterSms> bList){
        context = new WeakReference<Context>(ctx);
        betterSmsList = bList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_sms_row, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        holder.adapter=new BetterSmsAdapter(betterSmsList.get(position));
//        holder.recyclerView.setAdapter(holder.adapter);
    }

    @Override
    public int getItemCount() {
        return betterSmsList!=null?betterSmsList.size():0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        RecyclerView recyclerView;
        BetterSmsAdapter adapter;
        public ViewHolder(View itemView) {
            super(itemView);
            recyclerView =(RecyclerView) itemView.findViewById(R.id.sms_row);
        }
    }
}

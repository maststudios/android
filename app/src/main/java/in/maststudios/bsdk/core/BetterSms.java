package in.maststudios.bsdk.core;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by tanay.soley on 14/05/16.
 */
public class BetterSms {
    private String rawContent;
    private List<SmsRowItem> smsRowItems;
    private String selfParsed;
    private Date timestamp;
    private String senderNumber;
    private String senderName;
    private List<String> tags;
    private Catagory catagory;
    private String Heading;
    private boolean templatized;

    public String getRawContent() {
        return rawContent;
    }

    public void setRawContent(String rawContent) {
        this.rawContent = rawContent;
    }

    public List<SmsRowItem> getSmsRowItems() {
        return smsRowItems;
    }

    public void setSmsRowItems(List<SmsRowItem> smsRowItems) {
        this.smsRowItems = smsRowItems;
    }

    public String getSelfParsed() {
        return selfParsed;
    }

    public void setSelfParsed(String selfParsed) {
        this.selfParsed = selfParsed;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getSenderNumber() {
        return senderNumber;
    }

    public void setSenderNumber(String senderNumber) {
        this.senderNumber = senderNumber;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public Catagory getCatagory() {
        return catagory;
    }

    public void setCatagory(Catagory catagory) {
        this.catagory = catagory;
    }

    public String getHeading() {
        return Heading;
    }

    public void setHeading(String heading) {
        Heading = heading;
    }

    public boolean isTemplatized() {
        return templatized;
    }

    public void setTemplatized(boolean templatized) {
        this.templatized = templatized;
    }

    public enum Catagory {
        Conversation,
        Promotion,
        Transaction
    }

    public BetterSms(String senderNumber, String rawContent, Date timestamp) {
        this.timestamp = timestamp;
        this.senderNumber = senderNumber;
        this.rawContent = rawContent;
        smsRowItems = new LinkedList<>();
        templatize();
        if (!templatized) {
            selfParse();
        }
    }

    private void templatize() {
        List<Template> templates = TemplateManager.get().getTemplates(senderNumber);
        String parsedContentString = null;
        Template template = null;
        for (Template templ : templates) {
            parsedContentString = templ.getParsedContent(rawContent);
            if (parsedContentString != null) {
                template = templ;
                break;
            }
        }

        if (parsedContentString == null) {
            templatized = false;
            return;
        }

        templatized = true;

        //parsing parsed String
        String[] rows = parsedContentString.split("\n");
        for (int i = 0; i < rows.length; i++) {
            SmsRowItem smsRowItem = new SmsRowItem();
            String[] cols = rows[i].split(";");
            for (int j = 0; j < cols.length; j++) {

                //try{


                SmsColumnItem smsColumnItem = new SmsColumnItem();
                String[] properties = cols[j].split("~");
                if (properties[0].compareToIgnoreCase("small") == 0) {
                    smsColumnItem.setSize(SmsColumnItem.Size.Small);
                } else if (properties[0].compareToIgnoreCase("medium") == 0) {
                    smsColumnItem.setSize(SmsColumnItem.Size.Medium);
                } else if (properties[0].compareToIgnoreCase("large") == 0) {
                    smsColumnItem.setSize(SmsColumnItem.Size.Large);
                } else if (properties[0].compareToIgnoreCase("giant") == 0) {
                    smsColumnItem.setSize(SmsColumnItem.Size.Giant);
                } else if (properties[0].compareToIgnoreCase("verySmall") == 0) {
                    smsColumnItem.setSize(SmsColumnItem.Size.VerySmall);
                } else {
                    smsColumnItem.setSize(SmsColumnItem.Size.Medium);
                }

                if (properties[1].compareToIgnoreCase("none") == 0) {
                    smsColumnItem.setEmphasis(SmsColumnItem.Emphasis.None);
                } else if (properties[1].compareToIgnoreCase("info") == 0) {
                    smsColumnItem.setEmphasis(SmsColumnItem.Emphasis.Info);
                } else if (properties[1].compareToIgnoreCase("negative") == 0) {
                    smsColumnItem.setEmphasis(SmsColumnItem.Emphasis.Negative);
                } else if (properties[1].compareToIgnoreCase("positive") == 0) {
                    smsColumnItem.setEmphasis(SmsColumnItem.Emphasis.Positive);
                } else if (properties[1].compareToIgnoreCase("neutral") == 0) {
                    smsColumnItem.setEmphasis(SmsColumnItem.Emphasis.Neutral);
                } else {
                    smsColumnItem.setEmphasis(SmsColumnItem.Emphasis.None);
                }

                if (properties[2].compareToIgnoreCase("left") == 0) {
                    smsColumnItem.setAlignment(SmsColumnItem.Alignment.Left);
                } else if (properties[2].compareToIgnoreCase("center") == 0) {
                    smsColumnItem.setAlignment(SmsColumnItem.Alignment.Right);
                } else if (properties[2].compareToIgnoreCase("right") == 0) {
                    smsColumnItem.setAlignment(SmsColumnItem.Alignment.Center);
                } else {
                    smsColumnItem.setAlignment(SmsColumnItem.Alignment.Left);
                }

                smsColumnItem.setText(properties[3]);
                smsRowItem.getSmsColumnItems().add(smsColumnItem);
//                }catch (Exception e){
//                    e.printStackTrace();
//                    Log.e("Raw Content",rawContent);
//                    Log.e("Parsed Content",parsedContentString);
//                }
            }
            smsRowItems.add(smsRowItem);

        }
    }

    private void selfParse() {
        selfParsed = rawContent;
    }
}

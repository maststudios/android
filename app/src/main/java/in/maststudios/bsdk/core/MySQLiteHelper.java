package in.maststudios.bsdk.core;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import in.maststudios.bsdk.utils.BSDKApplication;

public class MySQLiteHelper extends SQLiteOpenHelper {

    private static MySQLiteHelper instance;

    public MySQLiteHelper() {
        super(BSDKApplication.getInstance().getContext(), "bsdk", null, 1);
    }

    private static final String TABLE_TEMPLATE="template";
    private static final String TABLE_TEMPLATE_COLUMN_SENDER="senderTmp";
    private static final String TABLE_TEMPLATE_COLUMN_RAW="rawTmp";
    private static final String TABLE_TEMPLATE_COLUMN_PARSED="parsedTmp";
    private static final String CREATE_TABLE_TEMPLATE = "create table "+TABLE_TEMPLATE+" (" +
                                                                                ""+TABLE_TEMPLATE_COLUMN_SENDER+" TEXT," +
                                                                                ""+TABLE_TEMPLATE_COLUMN_RAW+" TEXT," +
                                                                                ""+TABLE_TEMPLATE_COLUMN_PARSED+" TEXT"+
                                                                                ")";

    public static MySQLiteHelper getInstance(){
        if(instance==null){
            instance = new MySQLiteHelper();
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TEMPLATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE"+TABLE_TEMPLATE);
        onCreate(db);
    }

    public List<Template> getAllTemplates(){
        ArrayList<Template> templateList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor=db.query(TABLE_TEMPLATE,new String[]{TABLE_TEMPLATE_COLUMN_SENDER,TABLE_TEMPLATE_COLUMN_RAW,TABLE_TEMPLATE_COLUMN_PARSED},null,null,null,null,null);
        if(cursor.moveToFirst()){
            do{
                Template template = new Template(
                        cursor.getString(cursor.getColumnIndex(TABLE_TEMPLATE_COLUMN_SENDER)),
                        cursor.getString(cursor.getColumnIndex(TABLE_TEMPLATE_COLUMN_RAW)),
                        cursor.getString(cursor.getColumnIndex(TABLE_TEMPLATE_COLUMN_PARSED))
                );
                templateList.add(template);
            }while(cursor.moveToNext());
        }
        return templateList;
    }

    public void addTemplate(Template template){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TABLE_TEMPLATE_COLUMN_SENDER,template.getSenderNumberTemplate());
        cv.put(TABLE_TEMPLATE_COLUMN_RAW,template.getRawContentTemplate());
        cv.put(TABLE_TEMPLATE_COLUMN_PARSED,template.getParsedContentTemplate());
        db.insert(TABLE_TEMPLATE,null,cv);
    }
}
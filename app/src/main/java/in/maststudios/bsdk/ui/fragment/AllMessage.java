package in.maststudios.bsdk.ui.fragment;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import in.maststudios.bsdk.R;
import in.maststudios.bsdk.core.BetterSms;
import in.maststudios.bsdk.core.MySms;
import in.maststudios.bsdk.ui.adapter.BetterSmsAdapter;


public class AllMessage extends Fragment {

    BetterSmsAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    ArrayList<BetterSms> betterSmsList;
    String type;

    private static final int MY_PERMISSIONS_REQUEST_READ_SMS = 101;
    private static final String TYPE = "type";
    @Bind(R.id.sms_list_view)
    RecyclerView smsListView;

    public AllMessage() {
        // Required empty public constructor
    }

    public static AllMessage getAllMessageAdapter(String type) {
        AllMessage fragment = new AllMessage();
        Bundle args = new Bundle();
        args.putString(TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString(TYPE);
        }
    }

    private void initVars() {
        betterSmsList = new ArrayList<>();
        //betterSmsList.addAll(getBetterSms());
        adapter = new BetterSmsAdapter(getActivity(), betterSmsList);
        linearLayoutManager = new LinearLayoutManager(getActivity());
    }

    private void initUi() {
        smsListView.setAdapter(adapter);
        smsListView.setLayoutManager(linearLayoutManager);
        getBetterSms();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_message, container, false);
        ButterKnife.bind(this, view);
        initVars();
        initUi();
        return view;
    }

    public void getBetterSms() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED) {

//            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
//                    Manifest.permission.READ_CONTACTS)) {
//
//                // Show an expanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//
//            } else {
//
//                // No explanation needed, we can request the permission.
//
//
//
//                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                // app-defined int constant. The callback method gets the
//                // result of the request.
//            }
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_SMS},
                    MY_PERMISSIONS_REQUEST_READ_SMS);
        }else{
            getAllSms();
        }
       // ArrayList<BetterSms> list = new ArrayList<>();
        //list.add(new BetterSms("IY-ICICIB","Dear Customer, Your Ac XXXXXXXX3095 is debited with INR5,000.00  on 13 May. Info.EBA*MF-FRANKLIN-41601640*201. Your Total Avbl. Bal is INR15,361.35.",new Date()));

//        List<MySms> mySmses = getAllSms();
//        for(MySms sms:mySmses){
//            list.add(new BetterSms(sms.getAddress(),sms.getMsg(),new Date(Long.parseLong(sms.getTime()))));
//        }
//        BetterSms betterSms = new BetterSms("+98989898", "Hello namaste ab kaam ki baat pe aate he");
//        betterSms.setTemplatized(false);
//        betterSms.setSelfParsed("Hello namaste ab kaam ki baat pe aate he");
//
//        //betterSmsList.add(betterSms);
//
//

//        betterSms = new BetterSms("+98989898", "Hello namaste ab kaam ki baat pe aate he");
//        betterSms.setTemplatized(true);
//        betterSms.setSenderName("Lorem Sender");
//        betterSms.setSenderNumber("+98989898");
//
//        ArrayList<SmsRowItem> smsRowItems = new ArrayList<>();
//        SmsRowItem rowItem = new SmsRowItem();
//        ArrayList<SmsColumnItem> columnItems = new ArrayList<>();
//
//        SmsColumnItem smsColumnItem = new SmsColumnItem();
//        smsColumnItem.setEmphasis(SmsColumnItem.Emphasis.Info);
//        smsColumnItem.setSize(SmsColumnItem.Size.Medium);
//        smsColumnItem.setText("Bank");
//        columnItems.add(smsColumnItem);
//
//        smsColumnItem = new SmsColumnItem();
//        smsColumnItem.setEmphasis(SmsColumnItem.Emphasis.Info);
//        smsColumnItem.setSize(SmsColumnItem.Size.Medium);
//        smsColumnItem.setText("HDFC");
//        columnItems.add(smsColumnItem);
//
//        rowItem.setSmsColumnItems(columnItems);
//
//        smsRowItems.add(rowItem);
//
//        rowItem = new SmsRowItem();
//        smsColumnItem = new SmsColumnItem();
//        smsColumnItem.setEmphasis(SmsColumnItem.Emphasis.Negative);
//        smsColumnItem.setSize(SmsColumnItem.Size.Large);
//        smsColumnItem.setText("Credit");
//        columnItems.add(smsColumnItem);
//
//        smsColumnItem = new SmsColumnItem();
//        smsColumnItem.setEmphasis(SmsColumnItem.Emphasis.Negative);
//        smsColumnItem.setSize(SmsColumnItem.Size.Large);
//        smsColumnItem.setText("4000");
//        columnItems.add(smsColumnItem);
//
//        rowItem.setSmsColumnItems(columnItems);
//
//        smsRowItems.add(rowItem);
//
//
//        betterSms.setSmsRowItems(smsRowItems);
//
//        betterSmsList.add(betterSms);
//        return list;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void getAllSms() {

        List<MySms> lstSms = new ArrayList<MySms>();
        MySms objSms;
        Uri message = Uri.parse("content://sms/");
        ContentResolver cr = getActivity().getContentResolver();

        ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Cursor c = cr.query(message, null, null, null, null);
        int totalSMS = c.getCount();

        progressDialog.dismiss();

        if (c.moveToFirst()) {
            for (int i = 0; i < totalSMS; i++) {

                objSms = new MySms();
                objSms.setId(c.getString(c.getColumnIndexOrThrow("_id")));
                objSms.setAddress(c.getString(c
                        .getColumnIndexOrThrow("address")));
                objSms.setMsg(c.getString(c.getColumnIndexOrThrow("body")));
                objSms.setReadState(c.getString(c.getColumnIndex("read")));
                objSms.setTime(c.getString(c.getColumnIndexOrThrow("date")));
                if (c.getString(c.getColumnIndexOrThrow("type")).contains("1")) {
                    objSms.setFolderName("inbox");
                } else {
                    objSms.setFolderName("sent");
                }

                lstSms.add(objSms);
                betterSmsList.add(new BetterSms(objSms.getAddress(),objSms.getMsg(),new Date(Long.parseLong(objSms.getTime()))));
                adapter.notifyDataSetChanged();
                c.moveToNext();
            }
        }
        // else {
        // throw new RuntimeException("You have no SMS");
        // }
        c.close();

//        for(MySms sms:lstSms){
//            betterSmsList.add(new BetterSms(sms.getAddress(),sms.getMsg(),new Date(Long.parseLong(sms.getTime()))));
//        }
//        adapter.notifyDataSetChanged();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_SMS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getAllSms();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}

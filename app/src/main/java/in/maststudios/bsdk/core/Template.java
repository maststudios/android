package in.maststudios.bsdk.core;

import android.util.Log;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by tanay.soley on 14/05/16.
 */

public class Template {
    private String senderNumberTemplate;
    private String rawContentTemplate;
    private String parsedContentTemplate;

    public Template(String senderNumberTemplate, String rawContentTemplate, String parsedContentTemplate) {
        this.senderNumberTemplate = senderNumberTemplate;
        this.rawContentTemplate = rawContentTemplate;
        this.parsedContentTemplate = parsedContentTemplate;
    }

    public boolean matchSenderNumebr(String s) {
        if (s.length() != senderNumberTemplate.length()) {
            return false;
        }

        for (int i = 0; i < senderNumberTemplate.length(); i++) {
            if (senderNumberTemplate.charAt(i) != '$' && senderNumberTemplate.charAt(i) != s.charAt(i)) {
                return false;
            }
        }

        return true;
    }

    public String getParsedContent(String rawContent) {
        rawContent=rawContent+"~";
        String rawContentTemplate = this.rawContentTemplate+"~";
        Log.d("Parsed Content Template",parsedContentTemplate);
        String[] pieces = rawContentTemplate.split("%v");
        List<Integer[]> se = new LinkedList<>();
        int end = 0;
        for (int i = 0; i < pieces.length; i++) {
            Integer[] temp = new Integer[2];
            temp[0] = rawContent.indexOf(pieces[i], end);
            if (temp[0] == -1) {
                return null;
            }
            temp[1] = temp[0] + pieces[i].length() - 1;
            end = temp[1];
            se.add(temp);
        }

        List<String> variables = new LinkedList<>();
        for (int i=0;i<se.size()-1;i++) {
            variables.add(rawContent.substring(se.get(i)[1]+1, se.get(i+1)[0]));
        }

        pieces = parsedContentTemplate.split("%v()");
        String parsedContent = pieces[0];

        parsedContent = new String(parsedContentTemplate);
        for(int i=0;i<variables.size();i++) {
            String regex = "%v\\("+i+"\\)";
            parsedContent = parsedContent.replaceAll(regex,variables.get(i));
        }

        return parsedContent;
    }

    public String getSenderNumberTemplate() {
        return senderNumberTemplate;
    }

    public void setSenderNumberTemplate(String senderNumberTemplate) {
        this.senderNumberTemplate = senderNumberTemplate;
    }

    public String getRawContentTemplate() {
        return rawContentTemplate;
    }

    public void setRawContentTemplate(String rawContentTemplate) {
        this.rawContentTemplate = rawContentTemplate;
    }

    public String getParsedContentTemplate() {
        return parsedContentTemplate;
    }

    public void setParsedContentTemplate(String parsedContentTemplate) {
        this.parsedContentTemplate = parsedContentTemplate;
    }
}

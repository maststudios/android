package in.maststudios.bsdk.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import in.maststudios.bsdk.R;
import in.maststudios.bsdk.core.BetterSms;
import in.maststudios.bsdk.core.SmsColumnItem;
import in.maststudios.bsdk.core.SmsRowItem;
/**
 * Created by mishrabhilash on 5/14/16.
 */
public class BetterSmsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int TEMPLATIZED=0;
    public static final int UN_TEMPLATIZED=1;
    ArrayList<BetterSms> betterSmsList;
    static WeakReference<Context> context;
    public BetterSmsAdapter(Context ctx,ArrayList<BetterSms> betterSmsList) {
        this.betterSmsList = betterSmsList;
        context=new WeakReference<Context>(ctx);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=null;
        RecyclerView.ViewHolder vh=null;
        switch (viewType){
            case TEMPLATIZED:
                 v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_sms_detail_row_templatized, parent, false);
                // set the view's size, margins, paddings and layout parameters

                vh = new TemplateViewHolder(v);
                return vh;
            case UN_TEMPLATIZED:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.adapter_sms_detail_row_untemplatized, parent, false);
                // set the view's size, margins, paddings and layout parameters

                vh = new UnTemplateViewHolder(v);
                return vh;
            default:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.adapter_sms_detail_row_untemplatized, parent, false);
                // set the view's size, margins, paddings and layout parameters

                vh = new UnTemplateViewHolder(v);
                return vh;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BetterSms betterSms =betterSmsList.get(position);
        if(holder instanceof TemplateViewHolder){
            ((TemplateViewHolder) holder).smsDetailContainer.removeAllViews();
            List<SmsRowItem> rowItems = betterSms.getSmsRowItems();
            for (SmsRowItem row:rowItems) {
                List<SmsColumnItem> colItems = row.getSmsColumnItems();
                LinearLayout newRow = new LinearLayout(context.get());
                newRow.setOrientation(LinearLayout.HORIZONTAL);
                for (SmsColumnItem col : colItems)  {
                    TextView textView = new TextView(context.get());
                    textView.setText(col.getText());
                    customizeTextView(textView,col);
                    newRow.addView(textView);
                }
                ((TemplateViewHolder) holder).smsDetailContainer.addView(newRow);
            }
            ((TemplateViewHolder) holder).sender.setText(betterSms.getSenderName());
            ((TemplateViewHolder) holder).timeStamp.setText(betterSms.getTimestamp()+"");
        }else if(holder instanceof UnTemplateViewHolder){
            ((UnTemplateViewHolder) holder).sender.setText(betterSms.getSenderName());
            ((UnTemplateViewHolder) holder).timeStamp.setText(betterSms.getTimestamp()+"");
            ((UnTemplateViewHolder) holder).msg.setText(betterSms.getSelfParsed());
        }
    }

    @Override
    public int getItemCount() {
        return betterSmsList!=null?betterSmsList.size():0;
    }

    public class TemplateViewHolder extends RecyclerView.ViewHolder{
        TextView sender,timeStamp;
        LinearLayout smsDetailContainer;
        ImageButton seeRaw;
        public TemplateViewHolder(View itemView) {
            super(itemView);
            sender = (TextView)itemView.findViewById(R.id.sender);
            timeStamp = (TextView)itemView.findViewById(R.id.timestamp);
            smsDetailContainer = (LinearLayout)itemView.findViewById(R.id.sms_detail_container);
            seeRaw = (ImageButton)itemView.findViewById(R.id.see_raw);
            seeRaw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(context.get()).setMessage(betterSmsList.get(getAdapterPosition()).getRawContent())
                            .show();
                }
            });
        }
    }

    public class UnTemplateViewHolder extends RecyclerView.ViewHolder{
        TextView sender,timeStamp,msg;
        public UnTemplateViewHolder(View itemView) {
            super(itemView);
            sender = (TextView)itemView.findViewById(R.id.sender);
            timeStamp = (TextView)itemView.findViewById(R.id.timestamp);
            msg = (TextView)itemView.findViewById(R.id.msg);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(betterSmsList.get(position).isTemplatized()){
            return TEMPLATIZED;
        }else {
            return UN_TEMPLATIZED;
        }
    }

    private void customizeTextView(TextView textView,SmsColumnItem col) {
        //LinearLayout.LayoutParams textView.getLayoutParams()
        switch (col.getEmphasis()){
            case Info: textView.setTextColor(context.get().getResources().getColor(R.color.info));break;
            case Negative: textView.setTextColor(context.get().getResources().getColor(R.color.negative));break;
            case Neutral:textView.setTextColor(context.get().getResources().getColor(R.color.neutral));break;
            case Positive:textView.setTextColor(context.get().getResources().getColor(R.color.positive));break;
            case None:textView.setTextColor(context.get().getResources().getColor(R.color.default_color));break;
        }

        switch (col.getSize()){
            case Giant:textView.setTextSize(convertDpToPixel(20,context.get()));break;
            case Large:textView.setTextSize(convertDpToPixel(14,context.get()));break;
            case Small:textView.setTextSize(convertDpToPixel(8,context.get()));break;
            case Medium:textView.setTextSize(convertDpToPixel(11,context.get()));break;
            case VerySmall:break;
        }

        switch (col.getAlignment()){
            case Center:textView.setGravity(Gravity.CENTER);break;
            case Left:textView.setGravity(Gravity.LEFT);break;
            case Right:textView.setGravity(Gravity.RIGHT);break;
        }
    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }
}

package in.maststudios.bsdk.utils;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;

import timber.log.Timber;

/**
 * Created by mishrabhilash on 5/14/16.
 */
public class BSDKApplication extends Application {

    static BSDKApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();

        Timber.plant(new Timber.DebugTree(){
            @Override
            protected String createStackElementTag(StackTraceElement element) {
                return super.createStackElementTag(element)+":"+element.getMethodName()+":"+element.getLineNumber();
            }
        });

        Stetho.initializeWithDefaults(this);
        instance=this;
    }

    public static BSDKApplication getInstance()
    {
        return instance;
    }

    public Context getContext(){
        return instance.getApplicationContext();
    }
}

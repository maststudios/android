package in.maststudios.bsdk.core;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by tanay.soley on 14/05/16.
 */

public class SmsColumnItem {

    private Size size;
    private Emphasis emphasis;
    private String text;
    private Alignment alignment;

    public SmsColumnItem(){

    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Emphasis getEmphasis() {
        return emphasis;
    }

    public void setEmphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Alignment getAlignment() {
        return alignment;
    }

    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
    }


    public enum Size {
        VerySmall,
        Small,
        Medium,
        Large,
        Giant;
    }


    public enum Emphasis {
        None,
        Negative,
        Neutral,
        Positive,
        Info;
    }

    public enum Alignment{
        Left,
        Center,
        Right
    }
}

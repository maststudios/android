package in.maststudios.bsdk.core;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by tanay.soley on 14/05/16.
 */
public class TemplateManager {

    //TODO remove mocking

    private static TemplateManager templateManager;

    private TemplateManager() {
        mockTemplateList = new LinkedList<>();

        MySQLiteHelper.getInstance().addTemplate(new Template("$$-ICICIB", "Dear Customer, Your Ac %v is debited with INR%v  on %v. Info.%v. Your Total Avbl. Bal is INR%v.", "Small~None~Left~Transaction Alert\n" +
                "Small~None~Left~%v(2)\n" +
                "Small~None~Left~A/C\n" +
                "Small~Info~Left~%v(0)\n" +
                "Medium~None~Left~Debit\n" +
                "Giant~Negative~Left~%v(1)\n" +
                "Small~None~Left~Vendor: %v(3)\n" +
                "small~None~Left~Available Balance\n" +
                "large~info~left~%v(4)"));
        MySQLiteHelper.getInstance().addTemplate(new Template("121", "Bill Details For %v: Bill Date: %v Prev.Due: %v This Month's Due: %v Tot. Due: %v Due Date: %v Delivery Date: %v To view bill details or to pay online, click www.airtel.in/ccm/?p=m", "medium~none~left~Airtel Bill\n" +
                "small~none~left~%v(0)\n" +
                "small~none~left~Bill Date: %v(1)\n" +
                "small~none~left~Previous Due\n" +
                "medium~negative~left~%v(2)\n" +
                "small~none~left~This months due\n" +
                "medium~none~left~%v(3)\n" +
                "large~none~left~Total Due\n" +
                "giant~info~center~%v(4)\n" +
                "medium~none~left~Due Date\n" +
                "medium~info~left~%v(5)\n" +
                "small~none~left~Delivery Date\n" +
                "small~none~left~%v(6)"));
        MySQLiteHelper.getInstance().addTemplate(new Template("$$-ICICIB", "Dear Customer, You have made a Debit Card purchase of INR%v on %v. Info.%v. Your Net Available Balance is %v.", "medium~none~left~Debit Card Purchase\n" +
                "small~none~center~%v(1)\n" +
                "small~none~center~Amount\n" +
                "giant~info~center~\u20B9%v(0)\n" +
                "small~none~center~Available balance\n" +
                "large~info~center~%v(3)\n" +
                "small~none~left~Vendor\n" +
                "medium~none~left~%v(2)"));
        MySQLiteHelper.getInstance().addTemplate(new Template("$$-FPANDA", "FOOD-%v is your foodpanda verification code.Please enter the digits only.", "medium~none~left~Foodpanda Verification Code\n" +
                "giant~positive~left~%v(0)"));
        MySQLiteHelper.getInstance().addTemplate(new Template("$$-FPANDA", "Hi, We have picked up your order. We will notify you once our delivery person is in your neighborhood.", "medium~none~left~Foodpanda\n" +
                "large~positive~left~Orger Picked"));
        MySQLiteHelper.getInstance().addTemplate(new Template("$$-FPANDA", "Hi, Our delivery person is just around your address and will deliver the order shortly. If you don't receive it in next 15 mins, please call us on %v.", "medium~none~left~Foodpanda\n" +
                "small~none~left~Call us on\n" +
                "small~info~left~%v(0)\n" +
                "small~none~left~If you do not receive your order in 15 minutes"));
        MySQLiteHelper.getInstance().addTemplate(new Template("$$-FPANDA", "Beat the heat with foodpanda. Get Extra %v when you pay through wallet. Use Code: %v. Order Now: %v", "medium~none~left~Foodpanda\n" +
                "large~positive~left~%v(0)\n" +
                "small~none~left~When you pay through wallet\n" +
                "giant~info~left~%v(1)\n" +
                "medium~info~left~Order at %v(2)"));
        MySQLiteHelper.getInstance().addTemplate(new Template("$$-AIRMTA", "Bill payment for your mobile number  %v has been successful. Your account has been credited with Rs. %v. Trans Id  %v.Future reference Id %v.", "medium~none~left~Airtle bill payment successful\n" +
                "small~none~left~%v(0)\n" +
                "medium~none~left~Amount\n" +
                "giant~info~left~\u20B9%v(1)\n" +
                "small~info~left~Transaction id: %v(2)\n" +
                "small~info~left~Reference id: %v(3)"));
        MySQLiteHelper.getInstance().addTemplate(new Template("$$-AIRMTA", "Bill payment for your mobile number  %v has been successful. Your account has been credited with Rs. %v. Your reference is  %v", "medium~none~left~Airtle bill payment successful\n" +
                "small~none~left~%v(0)\n" +
                "medium~none~left~Amount\n" +
                "giant~info~left~\u20B9%v(1)\n" +
                "small~info~left~Reference id: %v(2)"));
        MySQLiteHelper.getInstance().addTemplate(new Template("$$-HDFCBK", "Thank you for using Debit Card ending %v for Rs.%v in %v at %v on %v  Avl bal: Rs.%v", "medium~none~left~HDFC Debit card Transaction\n" +
                "small~none~left~XXXX XXXX XXXX %v(0)\n" +
                "medium~none~left~Amount\n" +
                "giant~info~left~%v(1)\n" +
                "medium~none~left~Vendor: %v(3)\n" +
                "small~none~left~Location: %v(2)\n" +
                "small~none~left~Time %v(4)\n" +
                "medium~none~left~Balance\n" +
                "large~info~left~%v(5)"));
        MySQLiteHelper.getInstance().addTemplate(new Template("$$-HDFCBK", "OTP is %v for the txn of INR %v at %v on your card ending %v. Valid till %v. Do not share the OTP with anyone for security reasons", "medium~none~left~HDFC OTP\n" +
                "small~none~left~XXXX XXXX XXXX %v(3)\n" +
                "medium~none~left~Amount: \u20B9%v(1)\n" +
                "medium~none~left~Vendor: %v(3)\n" +
                "small~none~left~OTP\n" +
                "giant~none~left~%v(0)\n" +
                "verySmall~info~left~Do not share the OTP with anyone for security reasons"));
        //MySQLiteHelper.getInstance().addTemplate(new Template("", "", ""));

        mockTemplateList.addAll(MySQLiteHelper.getInstance().getAllTemplates());
    }

    public static TemplateManager get() {
        if (templateManager == null) {
            templateManager = new TemplateManager();
        }

        return templateManager;

    }

    //Mock Template list
    private List<Template> mockTemplateList;

    //Mocking the actual flow
    public List<Template> getTemplates(String senderNumber) {
        List<Template> ret = new LinkedList<>();
        for (Template template : mockTemplateList) {
            if (template.matchSenderNumebr(senderNumber)) {
                ret.add(template);
            }
        }
        return ret;
    }
}
